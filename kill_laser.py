from my_hardware import pwm_hat_controlled as phc

if __name__ == '__main__':
    # Convenience script to quickly turn the laser off if process hangs, etc.
    # TODO: Handle exits better to safely turn off laser in as many situations as possible
    lc = phc.LaserController(0, 1, 2)

    lc.off()
