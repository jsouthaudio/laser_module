import json
import random


class PathManager(object):
    def __init__(self, file: str = None):
        """Provides simple management of paths for the laser module

        :param str file: Path to a valid .json containing paths
        """
        # Stores the paths
        self._paths = {}  # type: dict

        if file:
            # File is specified, load the json, and store the paths
            with open(file) as f:
                self._paths = json.load(f)

    def __getitem__(self, key):
        """Overload"""
        return self._paths[key]

    @staticmethod
    def generate_random(num_points: int, pan_bounds: tuple = (0, 180), tilt_bounds: tuple = (0, 180),
                        speed_bounds: tuple = (30, 100)):
        """Generates a random path with a specified number of states, taking on random values in the specified ranges.

        :param int num_points: The number of states in the path
        :param (float, float) pan_bounds: (lower, upper) limits on the generated pan angles
        :param float, float) tilt_bounds: (lower, upper) limits on the generated tilt angles
        :param (float, float) speed_bounds: (lower, upper) limits on the generated speed values

        :return list : list of randomly generated states.
        """
        path = []

        for i in range(0, num_points):
            # Generate the values for each step, and append to the output list.
            pan = random.randint(*pan_bounds)
            tilt = random.randint(*tilt_bounds)
            speed = random.randint(*speed_bounds)

            path.append({'pan': pan, 'tilt': tilt, 'speed': speed, 'method': 'simultaneous', 'laser': True})

        return path
