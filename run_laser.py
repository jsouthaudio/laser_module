import os
import path_manager
from my_hardware import pwm_hat_controlled as phc

if __name__ == '__main__':

    # Define the laser controller
    lc = phc.LaserController(0, 1, 2)
    # Load some paths
    pm = path_manager.PathManager(os.path.join(os.path.dirname(__file__),'paths.json'))

    # number of repeats
    repeats = 3

    for i in range(0, repeats):
        lc.follow_path(pm['zigzag'])
        lc.follow_path(pm['zigzag'])
        lc.follow_path(pm['zagzig'])
        lc.follow_path(pm['zagzig'])
        lc.follow_path(pm['zigzag'])
        lc.follow_path(pm['zigzag'])
        lc.follow_path(pm.generate_random(50, (50, 105), (78, 110), (30, 100)))

    # Blink at the end, helps to calm down
    lc.follow_path(pm['blink'])
