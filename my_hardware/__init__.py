# Attempt to set up the hardware. Enter debug mode if any issues
try:
    import board
    import busio
    import adafruit_pca9685
    from adafruit_servokit import ServoKit

    i2c = busio.I2C(board.SCL, board.SDA)
    hat = adafruit_pca9685.PCA9685(i2c)
    hat.frequency = 60

    kit = ServoKit(channels=16)
except NotImplementedError:
    print('Hardware not connected - Entering Debug Mode')
    DEBUG = True

else:
    DEBUG = False


# Set up a some dummy hardware
class Channel(object):
    def __init__(self):
        self.duty_cycle = 0


class Hat(object):
    channels = [Channel()] * 16


class Servo(object):
    def __init__(self):
        self.angle = 90


class Kit(object):
    servo = [Servo()] * 16


# Define the dummy hat and kit if in debug mode.
if DEBUG:
    hat = Hat()
    kit = Kit()
