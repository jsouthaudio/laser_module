import numpy as np
import logging
from my_hardware import hat, kit

logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(module)s - %(funcName)s - %(message)s')


class Laser(object):
    def __init__(self, channel: int, power: float = 0.05) -> object:
        """Control for a 5V laser module, with switching/power controlled via the PWM duty cycle.

        :param int channel: PWM channel on the hat
        :param float power: Power as a % duty on range [0 1]
        """
        ## Initialise
        # Set the channel on the hat, storing a reference to the channel object on the hardware
        self._channel = hat.channels[channel]  # type: object

        self._power = 0.0  # type: int

        ## Setup
        # Set the initial power, and make sure the laser is off
        self.power = power
        self.off()

    @property
    def _hpower(self):
        """16 bit integer for setting the PWM duty, calculated from the power setting.
        :return int :
        """
        return int(self.power * (2 ** 16 - 1))

    @property
    def power(self):
        """Power for the laser
        :return float : Power as a % duty on range [0 1]
        """
        return self._power

    @power.setter
    def power(self, p):
        """Power setting for the laser

        :param float p: Power as a % duty on range [0 1]
        :return None : None
        """
        self._power = max(0.0, min(1.0, p))

    def on(self):
        """Turns the laser on

        :return None : None
        """
        self._channel.duty_cycle = self._hpower

    def off(self):
        """Turns the laser off

        :return None : None
        """
        self._channel.duty_cycle = 0


class TwoAxis(object):
    # Used to scale the speed
    _speed_scale = 500.0  # type: float

    def __init__(self, pan_channel: int, tilt_channel: int) -> object:
        """Controls for two servos set up as pan/tilt for two axis motion control

        :param int pan_channel: PWM channel on the hat, for the pan servo
        :param int tilt_channel: PWM channel on the hat, for the tilt servo
        """

        ## Initialise
        # Set the pan/tilt channels on the hat, storing a reference to the channel object on the hardware
        self._pan = kit.servo[pan_channel]  # type: object
        self._tilt = kit.servo[tilt_channel]  # type: object

        # Bound for pan/tilt. Pan/tilt angles will always be contained in these intervals.
        self._pan_bounds = (50, 105)  # type: tuple(float, float)
        self._tilt_bounds = (78, 110)  # type: tuple(float, float)

        # Centre point for pan/tilt angles
        self._centre_angles = ((self._pan_bounds[0] + self._pan_bounds[1]) / 2.0, \
                               (self._tilt_bounds[0] + self._tilt_bounds[1]) / 2.0)

        ## Setup
        # Centre the servo's on their operating range
        self._centre()

    @property
    def pan_angle(self):
        """Angle of the pan servo in degrees

        :return float : Returns the angle of the pan servo
        """
        return self._pan.angle

    @pan_angle.setter
    def pan_angle(self, theta: float):
        """Angle of the pan servo in degrees. Ensures the input is clipped to the pan bounds.

        :param float theta: Servo angle in degrees
        """
        self._pan.angle = max(self._pan_bounds[0], min(self._pan_bounds[1], theta))

    @property
    def tilt_angle(self):
        """Angle of the tilt servo in degrees

        :return float : Returns the angle of the tilt servo
        """
        return self._tilt.angle

    @tilt_angle.setter
    def tilt_angle(self, theta: float):
        """Angle of the tilt servo in degrees. Ensures the input is clipped to the tilt bounds.

        :param float theta: Servo angle in degrees
        """
        self._tilt.angle = max(self._tilt_bounds[0], min(self._tilt_bounds[1], theta))

    def _centre(self):
        """Sets the servos to the centre position
        :return None : None
        """
        self.pan_angle = self._centre_angles[0]
        self.tilt_angle = self._centre_angles[1]

    def go_to(self, state={'pan': 90.0, 'tilt': 90.0, 'speed': 100.0, 'method': 'simultaneous'}):
        """Moves servos from their current position, to a new position with the specified movement characteristics.

        Movement characteristics include the final position (pan/tilt angles), the movement speed, and whether servos
        move simultaneously or sequentially.

        :param dict state: Defines the final position and movement characteristics, key:value as follows
            'pan'    : float - Pan angle
            'tilt'   : float - Tilt angle
            'speed'  : float - Relative value on range [0, 100], defining the speed of movement
            'method' : str   - 'simultaneous' or 'sequential', sets whether servos move at the same time or separately

        :return None : None
        """

        # Number of steps for array. The higher the speed, the less number of steps. Scale by the overall _speed_scale
        # parameter
        steps = int(self._speed_scale * 100.0 / state['speed']) + 1

        # Create the arrays of intermediate pan/tilt angles
        pans = np.linspace(self.pan_angle, state['pan'], steps)[1:]
        tilts = np.linspace(self.tilt_angle, state['tilt'], steps)[1:]

        if state['method'] == 'simultaneous':
            # Move to each intermediate value, moving pan/tilt incrementally
            # TODO: Proper async control for simultaneous movement
            for p, t in zip(pans, tilts):
                self.pan_angle = p
                self.tilt_angle = t

        elif state['method'] == 'sequential':
            # First move pan through each value, then tilt.
            for p in pans:
                self.pan_angle = p
            for t in tilts:
                self.tilt_angle = t


class LaserController(Laser, TwoAxis):
    def __init__(self, laser_channel: int, pan_channel: int, tilt_channel: int):
        """Controls the entire two axis motion laser module.
        """
        # Initialise the superclasses, passing relevant channel to the constructors.
        Laser.__init__(self, laser_channel)
        TwoAxis.__init__(self, pan_channel, tilt_channel)

    def go_to(self, state={'pan': 90.0, 'tilt': 90.0, 'speed': 100.0, 'method': 'simultaneous', 'laser': False}):
        """Moves Laser module its current state, to a new state with the specified movement characteristics.

        Movement characteristics include the final position (pan/tilt angles), the movement speed, and whether servos
        move simultaneously or sequentially, and the laser state

        :param dict state: Defines the final position and movement characteristics, key:value as follows
            'pan'    : float - Pan angle
            'tilt'   : float - Tilt angle
            'speed'  : float - Relative value on range [0, 100], defining the speed of movement
            'method' : str   - 'simultaneous' or 'sequential', sets whether servos move at the same time or separately
            'laser'  : bool  - Laser state on (True) or off (False)

        :return None : None
        """
        logging.info('Going to state: {}'.format(state))

        # Move the servos
        super().go_to(state)

        # Set the laser state
        if state['laser']:
            self.on()
        else:
            self.off()

    def follow_path(self, path: list):
        """Moves the laser modules through an iterable of states, called a path.

        :param iter path: Iterable of states
        :return None : None
        """
        # Go to each state in the path
        for state in path:
            self.go_to(state)

        # Return to default state at the end
        self.go_to()
